import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

//element-ui：https://element.eleme.cn/
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

//可视化库：http://datav.jiaminghi.com/guide/
import dataV from '@jiaminghi/data-view'
Vue.use(dataV);

//日历
import FunctionalCalendar from "vue-functional-calendar";
Vue.use(FunctionalCalendar, {
  dayNames: ['一', '二', '三', '四', '五', '六', '日'],
  monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一", "十二"],
  shortMonthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一", "十二"]
});

import uicomp from "./../dvi/index";
import DataVi from "./../../packages";
//import "./../../lib/data-vi.css"
Vue.use(DataVi, uicomp)

import { configAxios } from "./../http";
configAxios();

new Vue({
  render: h => h(App),
}).$mount('#app')
