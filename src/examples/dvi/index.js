/*
此文件为自动生成，请在终端执行 npm run scan 命令进行更新
最后更新时间：Mon Oct 25 2021 14:07:03 GMT+0800 (中国标准时间)
*/

import A from "./A/index.vue";
import B from "./B/index.vue";
import Share from "./Share.vue";

const comps = [
    { name: "A", comp: A, file: "./A/index.vue"},
    { name: "B", comp: B, file: "./B/index.vue"},
    { name: "Share", comp: Share, file: "./Share.vue"}];

export function getComponents() {
    return comps;
}

export function install(app) {
    comps.forEach(ele => {
        app.component(ele.name, ele.comp);
    });
}

export default {
    getComponents,
    install,
}
