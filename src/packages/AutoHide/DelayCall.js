export default class DelayCall {
    constructor(callback, tick) {
        this._callback = callback;
        this._tick = tick;
        this._timer = null;
    }

    reset() {
        if (this._timer) clearTimeout(this._timer);
        this._timer = setTimeout(() => {
            this._callback()
        }, this._tick);
    }

    cancel() {
        if (this._timer) clearTimeout(this._timer);
    }
}