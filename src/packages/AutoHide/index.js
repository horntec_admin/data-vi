import AutoHideTop from "@/packages/AutoHide/AutoHideTop";
import AutoHideBottom from "@/packages/AutoHide/AutoHideBottom";
import AutoHideLeft from "@/packages/AutoHide/AutoHideLeft";
import AutoHideRight from "@/packages/AutoHide/AutoHideRight";

/**
 * 自动隐藏
 */
export default class AutoHide {

    /**
     * 新建一个可以自动隐藏的对象
     * @param element 需要绑定的对象
     * @param direction 隐藏方向 top, left, right, bottom 四个方向，外部需要自行控制定位，内部指挥修改这4个css的属性
     */
    constructor(element, direction) {
        if (!element) throw new Error("必须绑定到相关元素上");
        const dirs = ["top", "left", "right", "bottom"];
        if (dirs.indexOf(direction) < 0) throw new Error(`direction必须是 ${dirs.join(",")} 中的一个`);
        switch (direction) {
            case "top":
                this._worker = new AutoHideTop(element);
                break;
            case "left":
                this._worker = new AutoHideLeft(element);
                break;
            case "right":
                this._worker = new AutoHideRight(element);
                break;
            case "bottom":
                this._worker = new AutoHideBottom(element);
                break;
        }
        document.body.addEventListener("mousemove", e => {
            this._worker.update(e.clientX, e.clientY);
        });
    }

    /**
     * 开启自动隐藏功能
     */
    start() {
        this._worker.hide();
    }

    /**
     * 显示
     */
    show() {
        this._worker.show();
    }
}