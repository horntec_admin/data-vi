import DelayCall from "@/packages/AutoHide/DelayCall";

export default class AutoHideLeft {
    constructor(ele) {
        this._ele = ele;
        this._width = ele.getBoundingClientRect().width;
        this._hideTimer = new DelayCall(() => this.hide(), 500);
        this._isShow = true;
    }

    update(x, y) {
        if (x < this._width) {
            this._hideTimer.cancel();
            this.show();
        } else {
            this._hideTimer.reset();
        }
    }

    hide() {
        if (!this._isShow) return;
        this._isShow = false;
        this._ele.style.display = "none";
    }

    show() {
        if (this._isShow) return;
        this._isShow = true;
        this._ele.style.display = "";
    }
}
