import DelayCall from "./DelayCall";

export default class AutoHideTop {
    constructor(ele) {
        this._ele = ele;
        this._height = ele.getBoundingClientRect().height;
        this._hideTimer = new DelayCall(() => this.hide(), 3000);
        this._isShow = true;
    }

    update(x, y) {
        if (y < this._height) {
            this._hideTimer.cancel();
            this.show();
        } else {
            this._hideTimer.reset();
        }
    }

    hide() {
        if (!this._isShow) return;
        this._isShow = false;
        this._ele.style.display = "none";
    }

    show() {
        if (this._isShow) return;
        this._isShow = true;
        this._ele.style.display = "";
    }
}
