import dataVi from "../DataVi";
/**
*
*
* @export
* @class DataViManager
*/
export default class DataViManager {
    /**
    *   回调类型：{DataVProject[]}
    *
    * @static
    * @memberof DataViManager
    */
    static LoadAllProject() {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/LoadAllProject", {}, acc, err);
        });
    }
    /**
    *   回调类型：{string}
    *
    * @static
    * @param {DataVProject} project
    * @memberof DataViManager
    */
    static ModifyProject($project) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/ModifyProject", { "project": $project }, acc, err);
        });
    }
    /**
    *   回调类型：{void}
    *
    * @static
    * @param {string} idProject
    * @memberof DataViManager
    */
    static DeleteProject($idProject) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/DeleteProject", { "idProject": $idProject }, acc, err);
        });
    }
    /**
    *   回调类型：{ProjectDetail}
    *
    * @static
    * @param {string} friendlyName
    * @memberof DataViManager
    */
    static GetProject($friendlyName) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/GetProject", { "friendlyName": $friendlyName }, acc, err);
        });
    }
    /**
    *   回调类型：{ProjectDetail}
    *
    * @static
    * @param {string} idProject
    * @memberof DataViManager
    */
    static GetProjectById($idProject) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/GetProjectById", { "idProject": $idProject }, acc, err);
        });
    }
    /**
    *   回调类型：{string}
    *
    * @static
    * @param {DataVPage} page
    * @memberof DataViManager
    */
    static ModifyPage($page) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/ModifyPage", { "page": $page }, acc, err);
        });
    }
    /**
    *   回调类型：{void}
    *
    * @static
    * @param {string} idPage
    * @memberof DataViManager
    */
    static DeletePage($idPage) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/DeletePage", { "idPage": $idPage }, acc, err);
        });
    }
    /**
    *   回调类型：{DataVPage}
    *
    * @static
    * @param {string} idPage
    * @memberof DataViManager
    */
    static GetPage($idPage) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/GetPage", { "idPage": $idPage }, acc, err);
        });
    }
    /**
    *   回调类型：{void}
    *
    * @static
    * @param {string} idPage
    * @param {string} layout
    * @memberof DataViManager
    */
    static SavePageLayout($idPage, $layout) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/SavePageLayout", { "idPage": $idPage, "layout": $layout }, acc, err);
        });
    }
    /**
    *   回调类型：{DataVPage[]}
    *
    * @static
    * @param {string} idProject
    * @memberof DataViManager
    */
    static GetPages($idProject) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/GetPages", { "idProject": $idProject }, acc, err);
        });
    }
    /**
    *   回调类型：{void}
    *
    * @static
    * @param {string} idPage
    * @param {boolean} isPublished
    * @memberof DataViManager
    */
    static ChangePageState($idPage, $isPublished) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DataViManager/ChangePageState", { "idPage": $idPage, "isPublished": $isPublished }, acc, err);
        });
    }
}
