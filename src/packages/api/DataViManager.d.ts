/**
*
*
*
* @interface DataVProject
*/
interface DataVProject {
    /**
    *
    *
    *
    * @type string
    * @memberof DataVProject
    */
    IdProject: string;
    /**
    *
    *
    *
    * @type string
    * @memberof DataVProject
    */
    Name: string;
    /**
    *
    *
    *
    * @type string
    * @memberof DataVProject
    */
    FriendlyName: string;
    /**
    *
    *
    *
    * @type string
    * @memberof DataVProject
    */
    Description: string;
    /**
    *
    * 创建人姓名-除第一次外，其他时候最好不要动此字段的值
    *
    * @type string
    * @memberof DataVProject
    */
    CreateByName: string;
    /**
    *
    * 创建时间-除第一次外，其他时候最好不要动此字段的值
    *
    * @type Date
    * @memberof DataVProject
    */
    CreateAt: Date;
    /**
    *
    * 创建人编号-除第一次外，其他时候最好不要动此字段的值
    *
    * @type string
    * @memberof DataVProject
    */
    CreateId: string;
    /**
    *
    * 最后更新人姓名
    *
    * @type string
    * @memberof DataVProject
    */
    LastUpdateByName: string;
    /**
    *
    * 最后更新时间
    *
    * @type Date
    * @memberof DataVProject
    */
    LastUpdateAt: Date;
    /**
    *
    * 最后更新人编号
    *
    * @type string
    * @memberof DataVProject
    */
    LastUpdateId: string;
}
/**
*
*
*
* @interface DataVPage
*/
interface DataVPage {
    /**
    *
    *
    *
    * @type string
    * @memberof DataVPage
    */
    IdPage: string;
    /**
    *
    *
    *
    * @type string
    * @memberof DataVPage
    */
    IdProject: string;
    /**
    *
    *
    *
    * @type string
    * @memberof DataVPage
    */
    Name: string;
    /**
    *
    *
    *
    * @type number
    * @memberof DataVPage
    */
    OrderBy: number;
    /**
    *
    *
    *
    * @type string
    * @memberof DataVPage
    */
    Description: string;
    /**
    *
    *
    *
    * @type boolean
    * @memberof DataVPage
    */
    IsPublished: boolean;
    /**
    *
    *
    *
    * @type string
    * @memberof DataVPage
    */
    Layout: string;
    /**
    *
    * 创建人姓名-除第一次外，其他时候最好不要动此字段的值
    *
    * @type string
    * @memberof DataVPage
    */
    CreateByName: string;
    /**
    *
    * 创建时间-除第一次外，其他时候最好不要动此字段的值
    *
    * @type Date
    * @memberof DataVPage
    */
    CreateAt: Date;
    /**
    *
    * 创建人编号-除第一次外，其他时候最好不要动此字段的值
    *
    * @type string
    * @memberof DataVPage
    */
    CreateId: string;
    /**
    *
    * 最后更新人姓名
    *
    * @type string
    * @memberof DataVPage
    */
    LastUpdateByName: string;
    /**
    *
    * 最后更新时间
    *
    * @type Date
    * @memberof DataVPage
    */
    LastUpdateAt: Date;
    /**
    *
    * 最后更新人编号
    *
    * @type string
    * @memberof DataVPage
    */
    LastUpdateId: string;
}
/**
*
*
*
* @interface ProjectDetail
*/
interface ProjectDetail {
    /**
    *
    *
    *
    * @type DataVProject
    * @memberof ProjectDetail
    */
    Project: DataVProject;
    /**
    *
    *
    *
    * @type DataVPage[]
    * @memberof ProjectDetail
    */
    Pages: DataVPage[];
}
/**
*
*
* @export
* @class DataViManager
*/
export default class DataViManager {
    /**
    *   回调类型：{DataVProject[]}
    *
    * @static
    * @memberof DataViManager
    */
    static LoadAllProject(): Promise<DataVProject[]>;
    /**
    *   回调类型：{string}
    *
    * @static
    * @param {DataVProject} project
    * @memberof DataViManager
    */
    static ModifyProject($project: DataVProject): Promise<string>;
    /**
    *   回调类型：{void}
    *
    * @static
    * @param {string} idProject
    * @memberof DataViManager
    */
    static DeleteProject($idProject: string): Promise<void>;
    /**
    *   回调类型：{ProjectDetail}
    *
    * @static
    * @param {string} friendlyName
    * @memberof DataViManager
    */
    static GetProject($friendlyName: string): Promise<ProjectDetail>;
    /**
    *   回调类型：{ProjectDetail}
    *
    * @static
    * @param {string} idProject
    * @memberof DataViManager
    */
    static GetProjectById($idProject: string): Promise<ProjectDetail>;
    /**
    *   回调类型：{string}
    *
    * @static
    * @param {DataVPage} page
    * @memberof DataViManager
    */
    static ModifyPage($page: DataVPage): Promise<string>;
    /**
    *   回调类型：{void}
    *
    * @static
    * @param {string} idPage
    * @memberof DataViManager
    */
    static DeletePage($idPage: string): Promise<void>;
    /**
    *   回调类型：{DataVPage}
    *
    * @static
    * @param {string} idPage
    * @memberof DataViManager
    */
    static GetPage($idPage: string): Promise<DataVPage>;
    /**
    *   回调类型：{void}
    *
    * @static
    * @param {string} idPage
    * @param {string} layout
    * @memberof DataViManager
    */
    static SavePageLayout($idPage: string, $layout: string): Promise<void>;
    /**
    *   回调类型：{DataVPage[]}
    *
    * @static
    * @param {string} idProject
    * @memberof DataViManager
    */
    static GetPages($idProject: string): Promise<DataVPage[]>;
    /**
    *   回调类型：{void}
    *
    * @static
    * @param {string} idPage
    * @param {boolean} isPublished
    * @memberof DataViManager
    */
    static ChangePageState($idPage: string, $isPublished: boolean): Promise<void>;
}
export {};
