/**
*
* 扩展参数
*
* @interface ExtraArgument
*/
interface ExtraArgument {
    /**
    *
    * 参数名称
    *
    * @type string
    * @memberof ExtraArgument
    */
    Name: string;
    /**
    *
    * 参数值
    *
    * @type string
    * @memberof ExtraArgument
    */
    Value: string;
}
/**
* 通用的数据请求接口
*
* @export
* @class DvDataLoader
*/
export default class DvDataLoader {
    /**
    * 加载数据  回调类型：{any[]}
    *
    * @static
    * @param {string} script 脚本名称
    * @param {Date} begin 开始时间
    * @param {Date} end 结束时间
    * @param {ExtraArgument[]} extraArguments 其他扩展参数
    * @memberof DvDataLoader
    */
    static Load($script: string, $begin: Date, $end: Date, $extraArguments: ExtraArgument[]): Promise<any[]>;
}
export {};
