import dataVi from "../DataVi";
/**
* 通用的数据请求接口
*
* @export
* @class DvDataLoader
*/
export default class DvDataLoader {
    /**
    * 加载数据  回调类型：{any[]}
    *
    * @static
    * @param {string} script 脚本名称
    * @param {Date} begin 开始时间
    * @param {Date} end 结束时间
    * @param {ExtraArgument[]} extraArguments 其他扩展参数
    * @memberof DvDataLoader
    */
    static Load($script, $begin, $end, $extraArguments) {
        return new Promise((acc, err) => {
            dataVi._doRequest("DvDataLoader/Load", { "script": $script, "begin": $begin, "end": $end, "extraArguments": $extraArguments }, acc, err);
        });
    }
}
