import Datetime from './Datetime/index';
import Weather from "./weather/index";
import Empty from "./Empty/index";

const comps = [Datetime, Weather, Empty];

export default comps;