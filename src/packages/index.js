import dataVi from "./DataVi";
import Designer from '@/packages/Designer'
import Dashboard from '@/packages/Dashboard'
import innerUiComps from "./uiComps/index";

const components = [
    Designer,
    Dashboard
]
// 定义install方法
const install = function (Vue, uiComps) {
    Vue.prototype.$dv = dataVi;//全局对象

    uiComps.install(Vue);

    // 注册所有的组件
    components.forEach(component => {
        Vue.component(component.name, component)
    })

    let uic = [];
    innerUiComps.forEach(ele => {
        uic.push({ name: ele.name, comp: ele, file: "-" });
        Vue.component(ele.name, ele);
    });
    dataVi.useComponents(uic.concat(uiComps.getComponents()));
}
// 判断是否直接引入文件，如果是，就不用调用Vue.use()
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue)
}
// 导出install方法
export default {
    install,
    dataVi
}