function Today() {
    let now = new Date();
    return {
        begin: new Date(now.getFullYear(), now.getMonth(), now.getDate(), 0, 0, 0, 0),
        end: new Date(now.getFullYear(), now.getMonth(), now.getDate(), 23, 59, 59, 999)
    }
}

function CurrentWeek() {
    let now = new Date();
    let day = now.getDay();
    let date = now.getDate();
    let monday = date - day;
    return {
        begin: new Date(now.getFullYear(), now.getMonth(), monday, 0, 0, 0, 0),
        end: new Date(now.getFullYear(), now.getMonth(), monday + 6, 23, 59, 59, 999)
    }
}
function CurrentMonth() {
    let now = new Date();
    let date = now.getDate();
    return {
        begin: new Date(now.getFullYear(), now.getMonth(), 1, 0, 0, 0, 0),
        end: new Date(now.getFullYear(), now.getMonth(), date, 23, 59, 59, 999)
    }
}

function CurrentSeason() { 
    let now = new Date();
    now.setMonth(now.getMonth() - (now.getMonth() % 3));
     let end1=new Date(now.getFullYear(),now.getMonth()+3,1, 0, 0, 0, 0);
     end1.setMilliseconds(end1.getMilliseconds()-1); 
     let date = end1.getDate();      
    return {
        begin: new Date(now.getFullYear(), now.getMonth(), 1, 0, 0, 0, 0),
        end:new Date(end1.getFullYear(), end1.getMonth(), date, 23, 59, 59, 999),
    }    
}

function CurrentYear() { 
    let now = new Date();
    return {
        begin: new Date(now.getFullYear(), 0, 1, 0, 0, 0, 0),
        end: new Date(now.getFullYear(), 11, 31, 23, 59, 59, 999)
    }  
}

function LastMonth() {
    let now = new Date();
    now.setMonth(now.getMonth()-1);
    let bg=new Date(now.getFullYear(), now.getMonth(), 1, 0, 0, 0, 0);
    let end1=new Date(now.getFullYear(), now.getMonth(), 0, 0, 0, 0, 0);
    let date = end1.getDate();
    return {
        begin:bg,
        end: new Date(now.getFullYear(), now.getMonth(),date, 23, 59, 59, 999)
    }
}

function LastSeason() { 
    let now = new Date();
    now.setMonth(now.getMonth() - (now.getMonth() % 3));
    now.setMonth(now.getMonth()-3);
     let end1=new Date(now.getFullYear(),(now.getMonth()+3),0, 0, 0, 0, 0);
     let date = end1.getDate();         

    return {
        begin: new Date(now.getFullYear(), now.getMonth(), 1, 0, 0, 0, 0),
        end:new Date(end1.getFullYear(), end1.getMonth(), date, 23, 59, 59, 999),
    }    
}

function LastYear() { 
    let now = new Date();
    now.setFullYear(now.getFullYear() - 1);;
    return {
        begin: new Date(now.getFullYear(), 0, 1, 0, 0, 0, 0),
        end: new Date(now.getFullYear(), 11, 31, 23, 59, 59, 999)
    }  
}
const helper = {
    "本日": Today,
    "本周": CurrentWeek,
    "本月": CurrentMonth,
    "本季": CurrentSeason,
    "本年": CurrentYear,
    "上月": LastMonth,  
    "上季": LastSeason, 
    "上年": LastYear          
}

export default helper;