//模块与模块之间的间隙
const spacing = 10;

//最大行数
const maxRows = 6;

//列数
const colNum = 12;

/**
 * 依据父元素计算行高
 *
 * @param {*} ele
 * @return {*}
 */
function computeRowHeight(ele) {
    let parentRect = ele.getBoundingClientRect();
    let height =
        parentRect.bottom - parentRect.top - (maxRows + 1) * spacing;
    if (height < 0) {
        return 0;
    } else {
        return height / maxRows;
    }
}

export default {
    spacing,
    maxRows,
    colNum,
    computeRowHeight
}