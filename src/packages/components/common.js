import { Message } from 'element-ui';
import { Loading } from 'element-ui';

const uuid = {
    empty: () => {
        return '00000000-0000-0000-0000-000000000000';
    },
    newId: () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
};

function handlerError(e) {
    Message.warning(e.msg || e.message);
    console.warn(e);
}


let loadingInstance;
function loading(msg) {
    if (!msg) {
        if (loadingInstance) {
            loadingInstance.close();
            loadingInstance = null;
        }
        return;
    }
    if (loadingInstance) return;
    loadingInstance = Loading.service({
        lock: true,
        text: msg,
        spinner: "el-icon-loading",
        background: "rgba(0, 0, 0, 0.7)",
    });
}

const common = {
    uuid,
    handlerError,
    loading
};

export default common;