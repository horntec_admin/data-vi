/**
 * 加载外部js
 *
 * @export
 * @param {*} url js文件地址
 * @return {Promise} 
 */
export function loadJs(url) {
    return new Promise((acc, rej) => {
        let script = document.createElement("script");
        script.type = "text/javascript";
        script.src = url;
        script.onload = () => { acc(); };
        script.onerror = () => { rej(); };
        document.head.appendChild(script);
    });
}