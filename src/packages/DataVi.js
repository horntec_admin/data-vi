import DvDataLoader from "./api/DvDataLoader";
/**
 * 数据可视化访问组件
 *
 * @class DataVi
 */
class DataVi {

    /**
     * Creates an instance of DataVi.
     * @memberof DataVi
     */
    constructor() {
        this._isInDesignModel = false;
        this._callbacks = [];
        this._sizeChangeCallbacks = [];
        this._loadingCount = 0;
        this._requestor = {
            invoke: (data, api, acc, err) => {
                err(new Error("尚未设置请求库"));
            }
        };
        this._uiComps = [];
        let now = new Date();
        this._refreshOptions = {
            begin: new Date(new Date().setMonth(now.getMonth() - 1)),
            end: now
        };
        this._onLoadingCallback = () => {
        };
    }

    _doRequest(url, args, acc, err) {
        this._requestor.invoke(args, url, acc, err);
    }

    /**
     * 将工作模式修改为设计器模式
     *
     * @memberof DataVi
     */
    setDesignModel() {
        this._isInDesignModel = true;
    }

    /**
     * 获取当前组件是否工作在设计器模式下
     *
     * @return {Boolean}
     * @memberof DataVi
     */
    isInDesignModel() {
        return this._isInDesignModel;
    }

    /**
     * 增加刷新回调，当需要刷新数据时，回调函数会被执行
     *
     * @param {Function} callback 回调函数
     * @memberof DataVi
     */
    onRefresh(callback) {
        let index = this._callbacks.findIndex(a => a == callback);
        if (index >= 0) return;
        this._callbacks.push(callback);
    }

    /**
     * 注销刷新回调，注销后，再有新的刷新请求，回调函数将不会被执行
     *
     * @param {Function} callback 回调函数
     * @memberof DataVi
     */
    offRefresh(callback) {
        let index = this._callbacks.findIndex(a => a == callback);
        if (index < 0) return;
        this._callbacks.splice(index, 1);
    }

    /**
     * 获取刷新参数
     *
     * @return {RefreshOptions}
     * @memberof DataVi
     */
    getRefreshOptions() {
        return this._refreshOptions;
    }

    /**
     * 设置刷新参数，该操作会导致当前所有回调函数被执行
     *
     * @param {RefreshOptions} opts
     * @memberof DataVi
     */
    setRefreshOptions(opts) {
        this._refreshOptions = opts;
        this._callbacks.forEach(ele => {
            try {
                ele();
            } catch (e) {
                console.warn("触发刷新回调出错", e);
            }
        });
    }

    /**
     * 获取指定脚本在当前条件下的数据;
     * 注意:
     * 1. 除此之外，也可以通过 api/DvDataLoader/Load 接口进行获取
     * 2. 此方法将自动区分是否在设计器模式下，设计器模式下，不会返回任何数据
     * 3. 该方法返回多个数据集，为二维数组 [ 数据集1:[行1:{列:值,列:值}，行2:{列:值,列:值}]，数据集2:[行1:{列:值,列:值}，行2:{列:值,列:值}] ]
     *
     * @param { String } script 定义在 DviScripts/xx.xml 中的脚本名称
     * @param { Object } extArgs 需要传递给该脚本的其他参数 kv格式，script，begin，end为预设参数
     * @return { Promise<any[]> } 服务器返回数据
     * @memberof DataVi
     */
    getData(script, extArgs) {
        if (this._isInDesignModel) {
            return new Promise((acc, rej) => {
                acc([]);
            });
        }

        let extraArguments = [];
        if (extArgs) {
            for (var attr in extArgs) {
                extraArguments.push({
                    Name: attr,
                    Value: extArgs[attr]
                });
            }
        }

        return DvDataLoader.Load(script,
            this._refreshOptions.begin,
            this._refreshOptions.end,
            extraArguments);
    }

    /**
     * 触发界面大小发生变化事件
     *
     * @memberof DataVi
     */
    raiseSizeChangedEvent() {
        this._sizeChangeCallbacks.forEach(ele => {
            try {
                ele();
            } catch (e) {
                console.warn("触发页面大小变化事件出错", e);
            }
        });
    }

    /**
     * 关注大小变化事件
     *
     * @param {*} callback 回调函数
     * @memberof DataVi
     */
    onSizeChanged(callback) {
        let index = this._sizeChangeCallbacks.indexOf(callback);
        if (index >= 0) return;
        this._sizeChangeCallbacks.push(callback);
    }

    /**
     * 取消关注大小变化事件
     *
     * @param {*} callback 回调函数
     * @memberof DataVi
     */
    offSizeChanged(callback) {
        let index = this._sizeChangeCallbacks.indexOf(callback);
        if (index < 0) return;
        this._sizeChangeCallbacks.splice(index, 1);
    }

    /**
     *
     * 显示/取消显示 数据正在加载中的状态
     * @param {*} isLoading 是否正在加载中
     * @memberof DataVi
     */
    loading(isLoading) {
        if (isLoading) {
            this._loadingCount++;
        } else {
            this._loadingCount--;
        }
        if (this._loadingCount > 0) {
            this._onLoadingCallback(true);
        } else {
            this._onLoadingCallback(false);
        }
    }

    /**
     * 注册加载中回调函数
     *
     * @param {*} callback 当加载中切换时会被调用，正在加载 传入 true 否则为 false
     * @memberof DataVi
     */
    onLoadingRequest(callback) {
        this._onLoadingCallback = callback;
    }

    /**
     * 设置网络请求函数，该函数入参为  (data, api, acc, err) 无返回值
     *
     * @param {*} requestor 请求对象，一般可以直接使用 RequestFactory.getRequestor() 的返回值
     * @memberof DataVi
     */
    useRequestor(requestor) {
        this._requestor = requestor;
    }

    /**
     * 注册可视化设计器可用的组件列表
     *
     * @param {*} comps 组件列表，单个定义为：{ name: "xxx", comp: xxx, file: "xxx.vue"}
     * @memberof DataVi
     */
    useComponents(comps) {
        if (!(comps instanceof Array)) throw new Error("参数必须是合法的数组类型");
        this._uiComps = comps;
    }
}

const dataVi = new DataVi();
export default dataVi;
