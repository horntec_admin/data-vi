import { VueConstructor } from "vue";

/**
 * 可视化组件模块
 *
 * @export
 * @interface UiComponent
 */
export interface UiComponent {
    /**
     * 组件名称， 必须是组件内部定义的name属性
     *
     * @type {String}
     * @memberof UiComponent
     */
    name: String,

    /**
     * 组件构造函数 import 就是这里的值 from "./xxx.vue";
     *
     * @type {VueConstructor}
     * @memberof UiComponent
     */
    comp: VueConstructor,

    /**
     * 组件对应的文件，仅做调试使用
     *
     * @type {String}
     * @memberof UiComponent
     */
    file: String
}