/**
 * 刷新参数
 *
 * @export
 * @interface RefreshOptions
 */
 export interface RefreshOptions {
    /**
     * 开始时间
     *
     * @type {Date}
     * @memberof RefreshOptions
     */
    begin: Date,

    /**
     * 结束时间
     *
     * @type {Date}
     * @memberof RefreshOptions
     */
    end: Date
}