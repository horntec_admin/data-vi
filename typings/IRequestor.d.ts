/**
 * 网络请求工厂定义
 *
 * @export
 * @interface IRequestor
 */
 export interface IRequestor {

    /**
     * 调用远程接口
     *
     * @param {Object} data 参数
     * @param {String} api api地址
     * @param {Function<any>} acc 成功回调
     * @param {Function<Error>} err 失败回调
     * @memberof Requestor
     */
    invoke(data: Object, api: String, acc: Function<any>, err: Function<Error>): void;
}