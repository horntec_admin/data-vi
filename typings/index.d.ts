import { RefreshOptions } from "./RefreshOptions";
import { IRequestor } from "./IRequestor";
import { UiComponent } from "./UiComponent";
import Vue from "vue";

/**
 * 可视化框架
 *
 * @export
 * @class DataVi
 */
export class DataVi {

    /**
     * 将工作模式修改为设计器模式
     *
     * @memberof DataVi
     */
    setDesignModel(): void;

    /**
     * 获取当前组件是否工作在设计器模式下
     *
     * @return {*}  {Boolean}
     * @memberof DataVi
     */
    isInDesignModel(): Boolean;

    /**
     * 增加刷新回调，当需要刷新数据时，回调函数会被执行
     *
     * @param {() => void} callback 回调函数
     * @memberof DataVi
     */
    onRefresh(callback: () => void): void;

    /**
     * 注销刷新回调，注销后，再有新的刷新请求，回调函数将不会被执行
     *
     * @param {Function} callback 回调函数
     * @memberof DataVi
     */
    offRefresh(callback: Function): void;

    /**
     * 获取刷新参数
     *
     * @return {*}  {RefreshOptions}
     * @memberof DataVi
     */
    getRefreshOptions(): RefreshOptions;

    /**
     * 设置刷新参数，该操作会导致当前所有回调函数被执行
     *
     * @param {RefreshOptions} opts 刷新参数信息
     * @memberof DataVi
     */
    setRefreshOptions(opts: RefreshOptions): void;

    /**
     * 获取指定脚本在当前条件下的数据;
     * 注意: 
     * 1. 除此之外，也可以通过 DvDataLoader/Load 接口进行获取
     * 2. 此方法将自动区分是否在设计器模式下，设计器模式下，不会返回任何数据
     *
     * @param {String} script 定义在 DviScripts/xx.xml 中的脚本名称(仅限本框架对应的服务器端)
     * @return {*}  {Promise<any[]>} 服务器返回数据
     * @memberof DataVi
     */
    getData(script: String): Promise<any[]>;

    /**
     * 触发界面大小发生变化事件
     *
     * @memberof DataVi
     */
    raiseSizeChangedEvent(): void;

    /**
     * 关注大小变化事件
     *
     * @param {() => void} callback 回调函数
     * @memberof DataVi
     */
    onSizeChanged(callback: () => void): void;

    /**
     * 取消关注大小变化事件
     *
     * @param {() => void} callback 回调函数
     * @memberof DataVi
     */
    offSizeChanged(callback: () => void): void;

    /**
     * 显示/取消显示 数据正在加载中的状态
     *
     * @param {Boolean} isLoading 是否正在加载中
     * @memberof DataVi
     */
    loading(isLoading: Boolean): void;

    /**
     * 注册加载中回调函数
     *
     * @param {(isloading: boolean) => void} callback 回调函数，参数表示是否需要显示加载界面
     * @memberof DataVi
     */
    onLoadingRequest(callback: (isloading: boolean) => void): void;

    /**
     * 设置所有请求使用的请求工厂
     *
     * @param {IRequestor} requestor 请求工厂对象 内部网站框架可直接使用 RequestFactory.getRequestor() 的返回值
     * @memberof DataVi
     */
    useRequestor(requestor: IRequestor): void;

    /**
     * 注册可视化设计器可用的组件列表 
     *
     * @param {UiComponent[]} comps 组件列表，单个定义为：{ name: "xxx", comp: xxx, file: "xxx.vue"}
     * @memberof DataVi 
     */
    useComponents(comps: UiComponent[]): void;
}


declare module 'vue/types/vue' {
    interface Vue {
        /**
         * 当前 DataVi 对象
         *
         * @type {DataVi}
         * @memberof Vue
         */
        $dv: DataVi
    }
}

declare let dv: {
    dataVi: DataVi,
    install: (Vue, uiComps) => {}
}

export default dv;