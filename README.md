## 1. 依赖项
1. "element-ui": "^2.15.5",
2. "vue-functional-calendar": "^2.9.5",
3. "vue-grid-layout": "^2.3.12",
4. "@jiaminghi/data-view": "^2.10.0"

## 2. 起步

1. 在main.js中添加如下代码， 以下代码为此库依赖的第三方库：
```
//element-ui：https://element.eleme.cn/
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

//data-v：http://datav.jiaminghi.com/guide/
import dataV from '@jiaminghi/data-view'
Vue.use(dataV);

//calendar
import FunctionalCalendar from "vue-functional-calendar";
Vue.use(FunctionalCalendar, {
  dayNames: ['一', '二', '三', '四', '五', '六', '日'],
  monthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一", "十二"],
  shortMonthNames: ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一", "十二"]
});

```
2. 添加此库的相关代码
```
import DataVi from "data-vi";
import xx from "xxx"; // xx是通过运行 scan.js 得到 index.js 导出项目
import "data-vi/lib/data-vi.css";
Vue.use(DataVi, xx);
```

3. 如果通过 Vue.use(DataVi, xx)安装，则此步骤可以省略：
```
import xx from "xxx"; // xx是通过运行 scan.js 得到 index.js 导出项目
DataV.dataVi.useComponents(xx.getComponents());// xx可以通过运行 scan.js 得到
```

## 3. 添加设计器页面
在设计器页面添加如下引用
```
<dv-designer preview="/dashboard">
  <a href="/">Home</a>
</dv-designer>
```
建议该页面仅有一个组件，且为全屏页面
其中：
1. ```<dv-designer></dv-designer>```的内部内容将出现在左上角
2. preview 指定的路径将在点击 预览 按钮打开的新页面中被使用，默认会传递当前项目的名称作为参数 例如：/dashboard/proj1

## 4. 添加展示界面
在展示页面添加如下引用
```
<dv-dashboard project="abcde"></dv-dashboard>
```
建议该页面仅有一个组件，且为全屏页面
其中：
1. project 参数为可选参数，表示当前需要加载的 可视化项目 
2. 如果没有设置 project 参数，也可以从路由上获取，参数名仍然是 project

## 5. 服务器API准备
1. DataViManager/LoadAllProject 
   描述：用于获取系统中所有的可视化项目清单
   输入参数：无
   返回值：
   ```
   [{
    "IdProject": "cf59fe3f-07de-4102-950c-d6d1fb91e673",
    "Name": "abcd",
    "FriendlyName": "abcde",
    "Description": ""
   }]
   ```
2. DataViManager/ModifyProject
   描述：新增或者编辑已有项目
   输入参数：
   ```
   {
     "project":{
       Description: ""
       FriendlyName: "abcde"
       IdProject: "cf59fe3f-07de-4102-950c-d6d1fb91e673" //当输入 '00000000-0000-0000-0000-000000000000' 时表示是新项目
       Name: "abcd"
     }
   }
   ```
   返回值：返回编辑或者新增的项目编号
3. DataViManager/DeleteProject
   描述：删除项目
   输入参数：
   ```
   {
     "idProject:"被删除的项目编号"
   }
   ```
   返回值：无
4. DataViManager/GetProject
   描述：获取项目详情
   输入参数：
   ```
   {
     "friendlyName:"xxxx" 需要加载的项目 名称
   }
   ```
   返回值：
   ```
   {
     "Project": {
     "IdProject": "cf59fe3f-07de-4102-950c-d6d1fb91e673",
     "Name": "abcd",
     "FriendlyName": "abcde",
     "Description": ""
    },
    "Pages": [
      {
        "IdPage": "d5b11fad-d480-4e60-a9b3-604bf6e14895",
        "IdProject": "cf59fe3f-07de-4102-950c-d6d1fb91e673",
        "Name": "aaaaa",
        "OrderBy": 0,
        "Description": "",
        "IsPublished": true,
        "Layout": "[{\"x\":2,\"y\":0,\"w\":1,\"h\":1,\"i\":\"d1440205-ef7f-4ce4-80ae-077e5c26575e\",\"name\":\"A\",\"component\":\"A\",\"moved\":false},{\"x\":2,\"y\":1,\"w\":1,\"h\":1,\"i\":\"d1440205-ef7f-4ce4-80ae-077e5c265755\",\"name\":\"B\",\"component\":\"B\",\"moved\":false},{\"x\":3,\"y\":0,\"w\":5,\"h\":2,\"i\":\"d1440205-ef7f-4ce4-80ae-077e5c265756\",\"name\":\"Share\",\"component\":\"Share\",\"moved\":false}]"
      }
    ]
   }
   ```
5. DataViManager/GetProjectById
   描述：用于通过项目编号获取项目信息
   输入参数：
   ```
   {
     "idProject": "cf59fe3f-07de-4102-950c-d6d1fb91e673" 项目编号
   }
   ```
   返回值：同接口4
6. DataViManager/ModifyPage
   描述：新增或者编辑页面
   输入参数：
   ```
   {
     "page":{
       Description: "asdasd"
       IdPage: "d5b11fad-d480-4e60-a9b3-604bf6e14895" //当输入 '00000000-0000-0000-0000-000000000000' 时表示是新页面
       IdProject: "cf59fe3f-07de-4102-950c-d6d1fb91e673"
       IsPublished: true
       Name: "aaaaa"
       OrderBy: 0
     }
   }
   ```
   返回值：返回新增或者编辑页面的编号
7. DataViManager/DeletePage
   描述：删除页面
   输入参数：
   ```
   {
     "idPage": "d5b11fad-d480-4e60-a9b3-604bf6e14895"
   }
   ```
   返回值：无
8. DataViManager/GetPage
   描述：获取页面详情
   输入参数：
   ```
   {
     "idPage": "d5b11fad-d480-4e60-a9b3-604bf6e14895"
   }
   ```
   返回值：
   ```
    {
      "IdPage": "fb43fe61-26d1-4404-8184-ed84a59d2810",
      "IdProject": "cf59fe3f-07de-4102-950c-d6d1fb91e673",
      "Name": "aaaaafff",
      "OrderBy": 0,
      "Description": "asdasdf",
      "IsPublished": true,
      "Layout": null
    }
   ```
9. DataViManager/SavePageLayout
   描述：保存某个页面的布局信息
   输入参数：
   ```
   {
     "idPage":"页面编号",
     "layout":"布局信息 Json字符串"
   }
   ```
   返回值：无
10. DataViManager/GetPages
    描述： 用于获取某个项目的所有页面清单
    输入参数：
    ```
    {
      "idProject": "项目编号"
    }
    ```
    返回值：
    ```
    [
      {
      "IdPage": "d5b11fad-d480-4e60-a9b3-604bf6e14895",
      "IdProject": "cf59fe3f-07de-4102-950c-d6d1fb91e673",
      "Name": "aaaaa",
      "OrderBy": 0,
      "Description": "asdasd",
      "IsPublished": true,
      "Layout": "[{\"x\":2,\"y\":0,\"w\":1,\"h\":1,\"i\":\"d1440205-ef7f-4ce4-80ae-077e5c26575e\",\"name\":\"A\",\"component\":\"A\",\"moved\":false},{\"x\":2,\"y\":1,\"w\":1,\"h\":1,\"i\":\"d1440205-ef7f-4ce4-80ae-077e5c265755\",\"name\":\"B\",\"component\":\"B\",\"moved\":false},{\"x\":3,\"y\":0,\"w\":5,\"h\":2,\"i\":\"d1440205-ef7f-4ce4-80ae-077e5c265756\",\"name\":\"Share\",\"component\":\"Share\",\"moved\":false}]"
      }
    ]
    ```
11. DataViManager/ChangePageState
    描述：修改页面的发布状态
    输入参数：
    ```
    {
      "idPage:"页面编号",
      "isPublished": true/false 是否发布
    }
    ```
    返回值：无
12. DvDataLoader/Load
    描述：用于获取服务器端指定脚本名称的查询结果, 在内部框架加持下，该接口将查询位于 DviScripts 目录下的脚本，其中 bengin 和 end将传递到脚本中
    输入参数：
    ```
    {
      "script": "脚本名称",
      "begin": "开始时间",
      "end": "结束时间"
    }
    ```
    返回值：any[][] 例如：[ 数据集1:[行1:{列:值,列:值}，行2:{列:值,列:值}]，数据集2:[行1:{列:值,列:值}，行2:{列:值,列:值}] ]

## 6. 开始享受
1. 在scan扫描的根目录或子目录中添加vue文件
2. 在vue文件中添加组件注册信息
```
widget: {
    id: "d1440205-ef7f-4ce4-80ae-077e5c26575e",//组件编号，必须全局唯一
    displayName: "A", //组件显示名称，仅设计器有用
    desc: "A-desc", //组件描述，仅设计器有用
    multi: true/false //表示该组件是否支持在一个页面上添加多个
  }
```
3. vue样例
```
<template>
  <div>A</div>
</template>

<script>
export default {
  widget: {
    id: "d1440205-ef7f-4ce4-80ae-077e5c26575e",
    displayName: "A",
    desc: "A-desc",
  },
  name: "A",
};
</script>
```
vue文件中可以通过 this.$dv 或者访问 运行时相关功能
this.$dv支持的详细信息参考 [https://gitee.com/horntec_admin/data-vi/blob/master/typings/index.d.ts](https://gitee.com/horntec_admin/data-vi/blob/master/typings/index.d.ts "https://gitee.com/horntec_admin/data-vi/blob/master/typings/index.d.ts") 

## 7. 扫描框架可用的组件，并且生成清单文件
1. 在package.js中添加如下脚本
   ```
   "scripts": {
      "scan": "node node_modules/data-vi/scan.js src/xx", //其中src/xx表示可用组件的根目录
   }
   ```
2. 终端运行 ```npm run scan``` 之后将在 src/xx 目录下生成 index.js 文件
3. 在main.js 中引入 src/xx/index.js 然后调用 DataV.dataVi.useComponents(xx);

## 8. 错误解决方案
1. Cannot find module ‘node-sass’
   npm install node-sass@4.14.1
