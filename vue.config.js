const path = require('path')
module.exports = {
    pages: {
        desginer: {
            // 修改项目入口文件
            entry: 'src/examples/desginer/main.js',
            template: 'public/index.html',
            filename: 'desginer.html'
        },
        dashboard: {
            entry: 'src/examples/dashboard/main.js',
            template: 'public/index.html',
            filename: 'dashboard.html'
        }
    },
    // 扩展webpack配置,使webpages加入编译
    chainWebpack: config => {
        config.module
            .rule('js')
            .include.add(path.resolve(__dirname, 'packages')).end()
            .use('babel')
            .loader('babel-loader')
            .tap(options => {
                return options
            });
    },
    configureWebpack: config => {
        if (process.env.NODE_ENV === 'production') {
            // 为生产环境修改配置...
            Object.assign(config, {
                externals: {
                    "vue": "Vue",
                    "element-ui": "element-ui",
                    "vue-functional-calendar": "vue-functional-calendar",
                    "vue-grid-layout": "vue-grid-layout",
                    "@jiaminghi/data-view": "@jiaminghi/data-view"
                }
            });
        }

    }
}